<?xml version="1.0"?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd">
<book>
  <article id="ELEMENTS">
    <articleinfo>
      <title>Elements of design</title>
      <subtitle>Tutorial</subtitle>
      <xi:include xmlns:xi="http://www.w3.org/2001/XInclude" href="tutorial-elements-authors.xml" />
    </articleinfo>
    <abstract>
      <para>
        This tutorial will demonstrate the elements and principles of design which are normally taught to early art
        students in order to understand various properties used in art making. This is not an exhaustive list, so please
        add, subtract, and combine to make this tutorial more comprehensive.
      </para>
    </abstract>
    <informalfigure>
      <mediaobject>
        <imageobject>
          <imagedata fileref="elements-f01.svg"/>
        </imageobject>
      </mediaobject>
    </informalfigure>
    <sect1>
      <title>Elements of Design</title>
      <para>
        The following elements are the building blocks of design.
      </para>
      <sect2>
        <title>Line</title>
        <para>
          A line is defined as a mark with length and direction, created by a point that moves across a surface. A line
          can vary in length, width, direction, curvature, and color. Line can be two-dimensional (a pencil line on
          paper), or implied three-dimensional.
        </para>
        <informalfigure>
          <mediaobject>
            <imageobject>
              <imagedata fileref="elements-f02.svg"/>
            </imageobject>
          </mediaobject>
        </informalfigure>
      </sect2>
      <sect2>
        <title>Shape</title>
        <para>
          A flat figure, shape is created when actual or implied lines meet to surround a space. A change in color or
          shading can define a shape. Shapes can be divided into several types: geometric (square, triangle, circle) and
          organic (irregular in outline).
        </para>
        <informalfigure>
          <mediaobject>
            <imageobject>
              <imagedata fileref="elements-f03.svg"/>
            </imageobject>
          </mediaobject>
        </informalfigure>
      </sect2>
      <sect2>
        <title>Size</title>
        <para>
          This refers to variations in the proportions of objects, lines or shapes. There is a variation of sizes in
          objects either real or imagined.
        </para>
        <informalfigure>
          <mediaobject>
            <imageobject>
              <imagedata fileref="elements-f04.svg"/>
            </imageobject>
          </mediaobject>
        </informalfigure>
      </sect2>
      <sect2>
        <title>Space</title>
        <para>
          Space is the empty or open area between, around, above, below, or within objects. Shapes and forms are made by
          the space around and within them. Space is often called three-dimensional or two- dimensional. Positive space
          is filled by a shape or form. Negative space surrounds a shape or form.
        </para>
        <informalfigure>
          <mediaobject>
            <imageobject>
              <imagedata fileref="elements-f05.svg"/>
            </imageobject>
          </mediaobject>
        </informalfigure>
      </sect2>
      <sect2>
        <title>Color</title>
        <para>
          Color is the perceived character of a surface according to the wavelength of light reflected from it. Color
          has three dimensions: HUE (another word for color, indicated by its name such as red or yellow), VALUE (its
          lightness or darkness), INTENSITY (its brightness or dullness).
        </para>
        <informalfigure>
          <mediaobject>
            <imageobject>
              <imagedata fileref="elements-f06.svg"/>
            </imageobject>
          </mediaobject>
        </informalfigure>
      </sect2>
      <sect2>
        <title>Texture</title>
        <para>
          Texture is the way a surface feels (actual texture) or how it may look (implied texture). Textures are
          described by word such as rough, silky, or pebbly.
        </para>
        <informalfigure>
          <mediaobject>
            <imageobject>
              <imagedata fileref="elements-f07.svg"/>
            </imageobject>
          </mediaobject>
        </informalfigure>
      </sect2>
      <sect2>
        <title>Value</title>
        <para>
          Value is how dark or how light something looks. We achieve value changes in color by adding black or white to
          the color. Chiaroscuro uses value in drawing by dramatically contrasting lights and darks in a composition.
        </para>
        <informalfigure>
          <mediaobject>
            <imageobject>
              <imagedata fileref="elements-f08.svg"/>
            </imageobject>
          </mediaobject>
        </informalfigure>
      </sect2>
    </sect1>
    <sect1>
      <title>Principles of Design</title>
      <para>
        The principles use the elements of design to create a composition.
      </para>
      <sect2>
        <title>Balance</title>
        <para>
          Balance is a feeling of visual equality in shape, form, value, color, etc. Balance can be symmetrical or
          evenly balanced or asymmetrical and un-evenly balanced. Objects, values, colors, textures, shapes, forms,
          etc., can be used in creating a balance in a composition.
        </para>
        <informalfigure>
          <mediaobject>
            <imageobject>
              <imagedata fileref="elements-f09.svg"/>
            </imageobject>
          </mediaobject>
        </informalfigure>
      </sect2>
      <sect2>
        <title>Contrast</title>
        <para>
          Contrast is the juxtaposition of opposing elements
        </para>
        <informalfigure>
          <mediaobject>
            <imageobject>
              <imagedata fileref="elements-f10.svg"/>
            </imageobject>
          </mediaobject>
        </informalfigure>
      </sect2>
      <sect2>
        <title>Emphasis</title>
        <para>
          Emphasis is used to make certain parts of their artwork stand out and grab your attention. The center of
          interest or focal point is the place a work draws your eye to first.
        </para>
        <informalfigure>
          <mediaobject>
            <imageobject>
              <imagedata fileref="elements-f11.svg"/>
            </imageobject>
          </mediaobject>
        </informalfigure>
      </sect2>
      <sect2>
        <title>Proportion</title>
        <para>
          Proportion describes the size, location or amount of one thing compared to another.
        </para>
        <informalfigure>
          <mediaobject>
            <imageobject>
              <imagedata fileref="elements-f12.svg"/>
            </imageobject>
          </mediaobject>
        </informalfigure>
      </sect2>
      <sect2>
        <title>Pattern</title>
        <para>
          Pattern is created by repeating an element (line, shape or color) over and over again.
        </para>
        <informalfigure>
          <mediaobject>
            <imageobject>
              <imagedata fileref="elements-f13.svg"/>
            </imageobject>
          </mediaobject>
        </informalfigure>
      </sect2>
      <sect2>
        <title>Gradation</title>
        <para>
          Gradation of size and direction produce linear perspective. Gradation of color from warm to cool and tone from
          dark to light produce aerial perspective. Gradation can add interest and movement to a shape. A gradation from
          dark to light will cause the eye to move along a shape.
        </para>
        <informalfigure>
          <mediaobject>
            <imageobject>
              <imagedata fileref="elements-f14.svg"/>
            </imageobject>
          </mediaobject>
        </informalfigure>
      </sect2>
    </sect1>
    <sect1>
      <title>Composition</title>
      <para>
        The combining of distinct elements to form a whole.
      </para>
      <informalfigure>
        <mediaobject>
          <imageobject>
            <imagedata fileref="elements-f15.svg"/>
          </imageobject>
        </mediaobject>
      </informalfigure>
    </sect1>
    <sect1>
      <title>Bibliography</title>
      <para>
        This is a partial bibliography used to build this document (links may no longer work today or link to malicious
        sites now, click with care).
      </para>
      <itemizedlist>
        <listitem>
          <para>
            <ulink url="http://www.makart.com/resources/artclass/EPlist.html">http://www.makart.com/resources/artclass/EPlist.html</ulink>
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="http://www.princetonol.com/groups/iad/Files/elements2.htm">http://www.princetonol.com/groups/iad/Files/elements2.htm</ulink>
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="http://www.johnlovett.com/test.htm">http://www.johnlovett.com/test.htm</ulink>
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="http://digital-web.com/articles/elements_of_design/">http://digital-web.com/articles/elements_of_design/</ulink>
          </para>
        </listitem>
        <listitem>
          <para>
            <ulink url="http://digital-web.com/articles/principles_of_design/">http://digital-web.com/articles/principles_of_design/</ulink>
          </para>
        </listitem>
      </itemizedlist>
      <para>
        Special thanks to Linda Kim
        (<ulink url="http://www.coroflot.com/redlucite/">http://www.coroflot.com/redlucite/</ulink>) for helping me
        (<ulink url="http://www.rejon.org/">http://www.rejon.org/</ulink>) with this tutorial. Also, thanks to the Open
        Clip Art Library (<ulink url="https://openclipart.org/">https://openclipart.org/</ulink>) and the graphics
        people have submitted to that project.
      </para>
    </sect1>
  </article>
</book>
