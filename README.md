# Inkscape Documentation

[[_TOC_]]

This includes all files required for creating localized documentation for Inkscape.

At the time of this writing this includes:
 - Inkscape man pages (*man*)
 - Inkscape keyboard and mouse reference (*keys*)
 - Inkscape tutorials (*tutorials*)

It also has rules to create translation statistics (*statistics*).

## Translating Documentation in this repository

### What needs translation?

An overview about the status of translations in the Inkscape project is available at:

- https://inkscape.org/doc/devel/translations-statistics-master.html (for Inkscape master branch)
- https://inkscape.org/doc/devel/translations-statistics-1.3.x.html (for the 1.3.x series)

### How do I get the files?

If you feel comfortable using git and/or the GitLab webinterface, the preferred way is to fork this repository and work on a new branch in this fork.

Alternatively translators can download the files from this repository as a zip archive and translate the documentation po files for their language.
At a later point, we may add info here about different versions that need to be translated.

For non-programmers, the quickest option to obtain the files is to download the zip archive. For this, visit the [documentation repository](https://gitlab.com/inkscape/inkscape-docs/documentation) and choose the branch (= Inkscape version) you want to work on in the dropdown directly below the project description. Then click on the download arrow on the right, and select to 'download source code', in your favorite archive file format (e.g. zip). 

### Which files need to be translated?

You can find po files ready for translation in the following subdirectories:
- keys
- man
- tutorials/\<various tutorial subdirectories\>

If there isn't a file for your language yet, you can create a new one by copying the files 'keys.pot', 'man.pot' or '\<tutorial_name.pot\>' and renaming the copy to '<your_language_code>.po'.

### Is there anything important I need to know?

- The translation files for the manual contain some unusual formatting, e.g. <pre>C\<E\<lt\>defsE\<gt\>\></pre> Please leave these strings intact.
- For the tutorials, we also need translations of the header and footer SVGs and for some of the screenshot PNG files.
  To find these files, watch out for SVG and PNG files that have a file name that only differs by language codes.
  Some strings in the header SVG file are paths, not editable texts — this is to ensure that the text will render correctly to the end user.
  To translate it, you will have to recreate the text object yourself (use a generic sans-serif font with appropriate license, e.g. ‘DejaVu Sans’ or ‘Bistream Vera Sans’, in italic)
  and convert it to a path when you're done. Also consider the translucent ‘tutorial’ text path in the background.

### I'm ready. How do I submit my translations?

The preferred way is to create a merge request using the GitLab webinterface.

Alternatively you can create a new issue (https://gitlab.com/inkscape/inkscape-docs/documentation/-/issues) and attach your edited file.
You may add the tags 'documentation' and 'translation' to it.

If you need help with the process feel free to contact us in our chat room at https://chat.inkscape.org/channel/documentation

## Updating documentation

### Tutorials

To update tutorials, open the file `<tutorial name>.xml` in the subdirectory for the tutorial of your choice in the `tutorials` directory and edit away. Use the current syntax in the file as a guide.

### Man page

To update the man page entries, you need to edit the files `inkscape.pod.in` and `inkview.pod.in` in the `man` subdirectory. The syntax here is a bit exotic, but there [exists documentation](https://perldoc.perl.org/perlpod) for it. You can ignore the warning message at the beginning that says not to edit the file directly. That message transfers into the derived documents, which are the actual target for that message to appear.

### Keyboard shortcuts

To update the keyboard shortcuts, you need to edit the file `keys.xml` in the `keys` subdirectory. Use the already existing examples in the file as a guide for choosing the correct syntax.

## Generating documentation on your own computer

While GitLab offers a way to auto-generate documentation files to check the results, you may wish to generate the documentation files locally on your own computer to check whether your changes produce the desired result.

In order to do this, first, download the [zip file](https://gitlab.com/inkscape/inkscape-docs/documentation/-/archive/master/documentation-master.zip) for this repository and extract the archive to your disk, or (better) clone the repository via git.

If using Ubuntu (>= 20.04) – or a derivative like Linux Mint –, you need to use the following commands to install all the necessary packages for generating the documentation:

```
$ sudo add-apt-repository ppa:inkscape.dev/stable
$ sudo apt-get update
$ sudo apt-get install git make less wget unzip gettext po4a libxml2-utils \
                python3-libxml2 python3 python3-pip python3-lxml \
                python3-pexpect inkscape fonts-liberation tidy xsltproc
$ pip3 install --user --upgrade scour itstool
```

Do not include the `$` sign when pasting the commands line-by-line. This will also install the current stable Inkscape version from the ppa.

If you're using another operating system / Linux distribution, we may be able to provide help with finding the correct packages / software to install [in the chat](https://chat.inkscape.org/channel/documentation), but there's no guarantee that it will work.

Each of the subdirectories in the repository has a README with detailed information on how to use (including more info on software requirements).

Also you'll find a Makefile in each subdirectory for usage with GNU make (use `make help` for usage information).

If you're feeling lucky (or know that you have all requirements installed)
you can also use the convenience targets from the top level Makefile:

- use `make all` to generate all documentation
- use `make clean` to delete generated files again, for a clean new trial
- use `make help` to see additional usage information

*Note: As git does not track file modification times `make` might not be able to determine which targets need to be
 re-made after checking out new files. In order to re-make all files use `make --always-make` (or `make -B`).*

## Preparing documentation for a new release

### Fix hidden bugs
- Check the latest build log for `keys`, `man` and `tutorials` and look for `warning:` messages.
- If any try to resolve it.

### Run non-automated tasks

These are not run automatically during build, they are not part of the `make all` command. They must be started manually before a release.

- Make up the DocBook XML source code
  - Run `make pretty-xml` in `tutorials` directory. This makes only whitespaces changes.
  - Do not forget to update translatable files (see below).
- Update authors from git
  - Run `make authors` in `keys` directory and commit changes.
  - Run `make update-authors` in `tutorials` directory and commit changes.

### General (only major versions)

- create a new branch, e.g. '1.2.x'
- update the file `.gitlab-ci.yml` with the name of the Inkscape branch it refers to
- push to the repository
- CI should be doing the work now and will push the new files to https://gitlab.com/inkscape/inkscape-docs/documentation-export/-/tree/master/
- the inkscape.org website regularly pulls its information from there
- after ~ 24 hours, check whether the documentation is online, e.g. at https://inkscape.org/doc/devel/translations-statistics-1.1.x.html or https://inkscape.org/de/doc/keys-1.1.x.html
- in the Inkscape repository, make sure that menu links in the `Help` menu go to the branch's links

### Updating translatable files (not only for releases)

- **When?** Periodically, or when a new Inkscape version is close to being released. This is a manual process that needs to be repeated whenever there are new strings to translate.
- **How?**
  - In the main folder of this repository, run
    `make po`
  - commit the changed po and pot files to the repository
- Make sure to do this for all relevant branches!

### Making the new version of the documentation the default for the website (only major versions)
- In the documentation export repository, edit the file [redirects.re](https://gitlab.com/inkscape/inkscape-docs/documentation-export/-/blob/master/redirects.re), so it points links (for keyboard shortcut reference, Inkscape and Inkview man pages) that do not give any version to the current stable branch.

### Getting updated man pages and tutorial files into the main Inkscape repository (for both major and minor versions)
- **When?** Periodically, or when a new Inkscape version is close to being released. This is a manual process that needs to be repeated for each release.
- **How?**
  - Make sure that all documentation changes that are supposed to be in that Inkscape version are merged to the correct documentation repository branch.
  - Download the latest artifacts for the man page and the tutorials of the correct documentation repository branch.
  - Unpack the zip files to your computer.
  - Delete the 'export-website' directories from both, we do not need them.
  - Now make a merge request to the correct branch in the main Inkscape repository where you have replaced the tutorial files in /share/tutorials with the updated tutorial files from the tutorial artifacts zip file, and the man page files in /man with the files from the man page artifacts zip file. Do not delete any files, only overwrite with the newly created ones.
  - Exactly how you replace the files is up to you - you can either use the Web IDE, or work with a local copy of the Inkscape repository. Using the Web IDE will take a lot longer and is more error prone, since you need to upload and rename files many times, while just copying them over using a file browser will be much faster.
